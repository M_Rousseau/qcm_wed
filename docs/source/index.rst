.. pyqcm documentation master file, created by
   sphinx-quickstart on Mon Sep 18 09:54:42 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pyqcm : documentation (|version|)
=================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   intro
   models
   defining_cluster_models
   defining_models
   parameters
   cdmft
   vca
   hartree
   other_functions
   options
   parallel


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
