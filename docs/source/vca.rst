#####################################
The Variational Cluster Approximation
#####################################

This submodule provides functions that implements the Variational Cluster Approximation (VCA).
The class `VCA` implements the VCA method. Defining an instance of this class performs the
VCA procedure.

.. autoclass:: pyqcm.vca.VCA
    :members:

--------------------------------------
Other VCA-related functions
--------------------------------------

.. automodule:: pyqcm.vca
    :members:
    :exclude-members: VCA

